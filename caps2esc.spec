%global debug_package %{nil}
%global srcname caps2esc
Name: caps2esc
# Version as given by upstream
Version: 1.0.2
Release: 3%{dist}
License: MIT
Summary: transforming the most useless key ever in the most useful one
Url: https://gitlab.com/interception/linux/plugins/%{srcname}

# Sources can be obtained by
# git clone https://gitlab.com/interception/linux/plugins/caps2esc
# cd caps2esc
# tito build --tgz
Source0: %{name}-%{version}.tar.gz

BuildArch: x86_64

BuildRequires: cmake
BuildRequires: yaml-cpp-devel
BuildRequires: libevdev-devel
BuildRequires: systemd-devel
BuildRequires: gcc-g++

Requires: libevdev
Requires: yaml-cpp
Requires: glibc

%description
Put what's useless in its place
By moving the CAPSLOCK function to the far ESC location
Make what's useful comfortably present, just below your Pinky
By moving both ESC and CTRL functions to the CAPSLOCK location
%prep
%autosetup
%build
cmake -B build -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=%{buildroot}/usr
cmake --build build
%install
cd build
make install
%files
%{_bindir}/caps2esc
%changelog
* Tue Oct 18 2022 Dawid Stasiak <dawid.stasiak21@gmail.com> 1.0.2-3
- fix debug package (dawid.stasiak21@gmail.com)

* Mon Oct 17 2022 Dawid Stasiak <dawid.stasiak21@gmail.com> 1.0.2-2
- fix specfile (dawid.stasiak21@gmail.com)

* Sun Oct 16 2022 Dawid Stasiak <dawid.stasiak21@gmail.com> 1.0.2-1
- update specfile (dawid.stasiak21@gmail.com)
- add g++ as build dependency (dawid.stasiak21@gmail.com)

* Fri Oct 15 2022 Dawid Stasiak <dawid.stasiak21@gmail.com> 1.0.1-1
- update build dependency
* Fri Oct 14 2022 Dawid Stasiak <dawid.stasiak21@gmail.com> 1.0.0-1
- new package built with tito

